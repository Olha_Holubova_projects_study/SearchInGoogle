import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.GoogleSearchPage;

import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;

public class SearchImageResultCountTest {
    private WebDriver driver;

    @Before
    public void browserSetup() {
        chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @Test
    public void searchImageTabNotEmptyTest() {
        String SEARCH_PATTERN = "kitten";
        int result = new GoogleSearchPage(driver)
                .openPage()
                .search(SEARCH_PATTERN)
                .openImagesTab()
                .countResultImages();

        Assert.assertTrue(result > 0);
    }

    @After
    public void browserTearDown() {
        driver.quit();
        driver = null;
    }
}